# Changelog

## v1.1.0 (2024-05-19)

### Changed

- Updated `recommended` preset to include
  [`osvVulnerabilityAlerts`](https://docs.renovatebot.com/configuration-options/#osvvulnerabilityalerts). (#4)

### Fixed

- Updated `recommended` preset with
  [`"rebaseWhen": "behind-base-branch"`](https://docs.renovatebot.com/configuration-options/#rebasewhen).
  This is the intended behavior as almost all projects in this group require
  fast-forward commits for merge requests, but that alone does not always
  force a rebase. (#5)

## v1.0.1 (2024-05-10)

### Fixed

- Updated recommended preset to properly disable all rate limiting. (#3)

## v1.0.0 (2024-02-16)

Initial release
