# Renovate config presets

A collection of reusable Renovate [config presets](https://docs.renovatebot.com/config-presets/).

## Usage

Config presets can be used in a Renovate config `extends` like built in presets. For example, the following configures the recommended presets for an `npm` project, pinned to a specific version:

```json
{
  "$schema": "https://docs.renovatebot.com/renovate-schema.json",
  "extends": [
    "gitlab>gitlab-ci-utils/renovate-config//presets/recommended#1.0.0",
    "gitlab>gitlab-ci-utils/renovate-config//presets/npm-recommended#1.0.0"
  ]
}
```

For complete details on how to use Renovate config presets, see the [Renovate documentation](https://docs.renovatebot.com/config-presets/).

## Config presets

All presets configs are located in the `presets` directory.

### [`docker-alpine`](./presets/docker-alpine.json)

Manage Alpine Linux version in container images, such as
`golang:1.22.0-alpine3.18` (updating to a Alpine 3.19, or later). The base
version is still managed by Renovate (in this case `golang:1.22.0`).

### [`docker-os-package`](./presets/docker-os-package.json)

Update OS packages in `Dockerfiles` based on variables, as shown below.

```Dockerfile
# renovate: datasource=repology depName=debian_12/chromium versioning=loose
ENV CHROMIUM_VERSION="121.0.6167.139-1~deb12u1"
RUN apt-get -y install --no-install-recommends chromium=${CHROMIUM_VERSION}
```

> Note that most OS package managers don't retain all package versions,
> so pinning package versions could result in a build failure until the project
> is updated to an available version.

### [`docker-pin-digests`](./presets/docker-pin-digests.json)

Pin docker digests only if the tag starts with a numbered version, such as
`v1.0.0` or `1.0.0`. This allows for the use of `latest`, `alpine`, etc. where
appropriate.

### [`go-install`](./presets/go-install.json)

Manages installed Go modules such as:

```yaml
job:
  before_script:
    - go install github.com/boumenot/gocover-cobertura@v1.0.0
```

### [`npm-install`](./presets/npm-install.json)

Manages globally installed `npm` packages in GitLab CI jobs, such as:

```yaml
job:
  script:
    - npm install -g lockfile-lint@4.11.0
```

### [`npm-recommended`](./presets/npm-recommended.json)

Set of recommended presets for `npm` packages (ignore `peerDependencies`,
ignore `engines`, include monthly lockfile maintenance, group
`minor`/`patch` `devDependencies`). This is an addition to the
`recommended` preset.

### [`pip-install`](./presets/pip-install.json)

Manages globally installed `pip` packages in GitLab CI jobs, such as:

```yaml
job:
  script:
    - pip install yamllint==1.33.0
```

### [`playwright`](./presets/playwright.json)

Rules to group updates to `playwright` npm packages and container image tags,
which must be updated together.

### [`recommended`](./presets/recommended.json)

The recommended base preset for all projects (sets timezone, labels, and
other defaults). Includes custom manager for Renovate config presets.

### [`vale-packages`](./presets/vale-packages.json)

Manages versioned `vale` packages in `.vale.ini` files, such as:

```ini
Packages = Google, \
  https://gitlab.com/gitlab-ci-utils/vale-package-gciu/-/releases/1.0.1/downloads/GCIU.zip

[*]
BasedOnStyles = Vale, Google, GCIU
```
